<?php

/**
 * @file
 * uw_ct_feds_hp_offerings.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_feds_hp_offerings_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'feds_hp_offerings';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Feds Homepage Offerings';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Feds Homepage Offerings';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_type'] = '0';
  $handler->display->display_options['fields']['body']['element_label_type'] = '0';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['body']['element_wrapper_class'] = 'offerings-icon';
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  /* Field: Content: Category Title */
  $handler->display->display_options['fields']['field_category_title']['id'] = 'field_category_title';
  $handler->display->display_options['fields']['field_category_title']['table'] = 'field_data_field_category_title';
  $handler->display->display_options['fields']['field_category_title']['field'] = 'field_category_title';
  $handler->display->display_options['fields']['field_category_title']['label'] = '';
  $handler->display->display_options['fields']['field_category_title']['element_type'] = '0';
  $handler->display->display_options['fields']['field_category_title']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_category_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_category_title']['element_wrapper_type'] = 'h3';
  $handler->display->display_options['fields']['field_category_title']['element_wrapper_class'] = 'offerings-cat-title';
  $handler->display->display_options['fields']['field_category_title']['element_default_classes'] = FALSE;
  /* Field: Content: Category Description */
  $handler->display->display_options['fields']['field_category_description']['id'] = 'field_category_description';
  $handler->display->display_options['fields']['field_category_description']['table'] = 'field_data_field_category_description';
  $handler->display->display_options['fields']['field_category_description']['field'] = 'field_category_description';
  $handler->display->display_options['fields']['field_category_description']['label'] = '';
  $handler->display->display_options['fields']['field_category_description']['alter']['max_length'] = '220';
  $handler->display->display_options['fields']['field_category_description']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_category_description']['alter']['html'] = TRUE;
  $handler->display->display_options['fields']['field_category_description']['element_type'] = '0';
  $handler->display->display_options['fields']['field_category_description']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_category_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_category_description']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_category_description']['element_wrapper_class'] = 'offerings-cat-desript';
  $handler->display->display_options['fields']['field_category_description']['element_default_classes'] = FALSE;
  /* Field: Content: Category Link */
  $handler->display->display_options['fields']['field_category_link']['id'] = 'field_category_link';
  $handler->display->display_options['fields']['field_category_link']['table'] = 'field_data_field_category_link';
  $handler->display->display_options['fields']['field_category_link']['field'] = 'field_category_link';
  $handler->display->display_options['fields']['field_category_link']['label'] = '';
  $handler->display->display_options['fields']['field_category_link']['element_type'] = '0';
  $handler->display->display_options['fields']['field_category_link']['element_label_type'] = 'div';
  $handler->display->display_options['fields']['field_category_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_category_link']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_category_link']['element_wrapper_class'] = 'offerings-cat-link';
  $handler->display->display_options['fields']['field_category_link']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_category_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_category_link']['settings'] = array(
    'custom_title' => '',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  /* Filter criterion: Content: Published status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_offerings' => 'feds_offerings',
  );

  /* Display: Feds Offerings Homepage Block */
  $handler = $view->new_display('block', 'Feds Offerings Homepage Block', 'block');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'What We Offer';
  $translatables['feds_hp_offerings'] = array(
    t('Master'),
    t('Feds Homepage Offerings'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Feds Offerings Homepage Block'),
    t('What We Offer'),
  );
  $export['feds_hp_offerings'] = $view;

  return $export;
}
