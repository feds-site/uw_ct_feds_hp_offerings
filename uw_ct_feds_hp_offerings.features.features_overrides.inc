<?php

/**
 * @file
 * uw_ct_feds_hp_offerings.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_feds_hp_offerings_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: user_permission
  $overrides["user_permission.create feds_offerings content.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.create feds_offerings content.roles|administrator"] = 'administrator';
  $overrides["user_permission.create feds_offerings content.roles|content author"] = 'content author';
  $overrides["user_permission.create feds_offerings content.roles|content editor"] = 'content editor';
  $overrides["user_permission.create feds_offerings content.roles|site manager"] = 'site manager';
  $overrides["user_permission.delete any feds_offerings content.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.delete any feds_offerings content.roles|administrator"] = 'administrator';
  $overrides["user_permission.delete any feds_offerings content.roles|content editor"] = 'content editor';
  $overrides["user_permission.delete any feds_offerings content.roles|site manager"] = 'site manager';
  $overrides["user_permission.delete own feds_offerings content.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.delete own feds_offerings content.roles|administrator"] = 'administrator';
  $overrides["user_permission.delete own feds_offerings content.roles|content editor"] = 'content editor';
  $overrides["user_permission.delete own feds_offerings content.roles|site manager"] = 'site manager';
  $overrides["user_permission.edit any feds_offerings content.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.edit any feds_offerings content.roles|administrator"] = 'administrator';
  $overrides["user_permission.edit any feds_offerings content.roles|content author"] = 'content author';
  $overrides["user_permission.edit any feds_offerings content.roles|content editor"] = 'content editor';
  $overrides["user_permission.edit any feds_offerings content.roles|site manager"] = 'site manager';
  $overrides["user_permission.edit own feds_offerings content.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.edit own feds_offerings content.roles|administrator"] = 'administrator';
  $overrides["user_permission.edit own feds_offerings content.roles|content author"] = 'content author';
  $overrides["user_permission.edit own feds_offerings content.roles|content editor"] = 'content editor';
  $overrides["user_permission.edit own feds_offerings content.roles|site manager"] = 'site manager';
  $overrides["user_permission.enter feds_offerings revision log entry.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.enter feds_offerings revision log entry.roles|administrator"] = 'administrator';
  $overrides["user_permission.enter feds_offerings revision log entry.roles|content author"] = 'content author';
  $overrides["user_permission.enter feds_offerings revision log entry.roles|content editor"] = 'content editor';
  $overrides["user_permission.enter feds_offerings revision log entry.roles|site manager"] = 'site manager';
  $overrides["user_permission.override feds_offerings authored by option.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.override feds_offerings authored by option.roles|administrator"] = 'administrator';
  $overrides["user_permission.override feds_offerings authored by option.roles|content author"] = 'content author';
  $overrides["user_permission.override feds_offerings authored by option.roles|content editor"] = 'content editor';
  $overrides["user_permission.override feds_offerings authored by option.roles|site manager"] = 'site manager';
  $overrides["user_permission.override feds_offerings authored on option.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.override feds_offerings authored on option.roles|administrator"] = 'administrator';
  $overrides["user_permission.override feds_offerings authored on option.roles|content author"] = 'content author';
  $overrides["user_permission.override feds_offerings authored on option.roles|content editor"] = 'content editor';
  $overrides["user_permission.override feds_offerings authored on option.roles|site manager"] = 'site manager';
  $overrides["user_permission.override feds_offerings promote to front page option.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.override feds_offerings promote to front page option.roles|administrator"] = 'administrator';
  $overrides["user_permission.override feds_offerings promote to front page option.roles|content author"] = 'content author';
  $overrides["user_permission.override feds_offerings promote to front page option.roles|content editor"] = 'content editor';
  $overrides["user_permission.override feds_offerings promote to front page option.roles|site manager"] = 'site manager';
  $overrides["user_permission.override feds_offerings published option.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.override feds_offerings published option.roles|administrator"] = 'administrator';
  $overrides["user_permission.override feds_offerings published option.roles|content author"] = 'content author';
  $overrides["user_permission.override feds_offerings published option.roles|content editor"] = 'content editor';
  $overrides["user_permission.override feds_offerings published option.roles|site manager"] = 'site manager';
  $overrides["user_permission.override feds_offerings revision option.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.override feds_offerings revision option.roles|administrator"] = 'administrator';
  $overrides["user_permission.override feds_offerings revision option.roles|content author"] = 'content author';
  $overrides["user_permission.override feds_offerings revision option.roles|content editor"] = 'content editor';
  $overrides["user_permission.override feds_offerings revision option.roles|site manager"] = 'site manager';
  $overrides["user_permission.override feds_offerings sticky option.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.override feds_offerings sticky option.roles|administrator"] = 'administrator';
  $overrides["user_permission.override feds_offerings sticky option.roles|content author"] = 'content author';
  $overrides["user_permission.override feds_offerings sticky option.roles|content editor"] = 'content editor';
  $overrides["user_permission.override feds_offerings sticky option.roles|site manager"] = 'site manager';
  $overrides["user_permission.search feds_offerings content.roles|anonymous user"] = 'anonymous user';
  $overrides["user_permission.search feds_offerings content.roles|authenticated user"] = 'authenticated user';

 return $overrides;
}
