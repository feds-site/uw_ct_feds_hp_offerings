<?php

/**
 * @file
 * uw_ct_feds_hp_offerings.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_feds_hp_offerings_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_homepage_offerings';
  $context->description = '';
  $context->tag = 'Feds';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-feds_hp_offerings-block' => array(
          'module' => 'views',
          'delta' => 'feds_hp_offerings-block',
          'region' => 'feds_offerings',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds');
  $export['feds_homepage_offerings'] = $context;

  return $export;
}
