<?php

/**
 * @file
 * uw_ct_feds_hp_offerings.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_hp_offerings_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create feds_offerings content'.
  $permissions['create feds_offerings content'] = array(
    'name' => 'create feds_offerings content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any feds_offerings content'.
  $permissions['delete any feds_offerings content'] = array(
    'name' => 'delete any feds_offerings content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own feds_offerings content'.
  $permissions['delete own feds_offerings content'] = array(
    'name' => 'delete own feds_offerings content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any feds_offerings content'.
  $permissions['edit any feds_offerings content'] = array(
    'name' => 'edit any feds_offerings content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own feds_offerings content'.
  $permissions['edit own feds_offerings content'] = array(
    'name' => 'edit own feds_offerings content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter feds_offerings revision log entry'.
  $permissions['enter feds_offerings revision log entry'] = array(
    'name' => 'enter feds_offerings revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_offerings authored by option'.
  $permissions['override feds_offerings authored by option'] = array(
    'name' => 'override feds_offerings authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_offerings authored on option'.
  $permissions['override feds_offerings authored on option'] = array(
    'name' => 'override feds_offerings authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_offerings comment setting option'.
  $permissions['override feds_offerings comment setting option'] = array(
    'name' => 'override feds_offerings comment setting option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_offerings promote to front page option'.
  $permissions['override feds_offerings promote to front page option'] = array(
    'name' => 'override feds_offerings promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_offerings published option'.
  $permissions['override feds_offerings published option'] = array(
    'name' => 'override feds_offerings published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_offerings revision option'.
  $permissions['override feds_offerings revision option'] = array(
    'name' => 'override feds_offerings revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_offerings sticky option'.
  $permissions['override feds_offerings sticky option'] = array(
    'name' => 'override feds_offerings sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search feds_offerings content'.
  $permissions['search feds_offerings content'] = array(
    'name' => 'search feds_offerings content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
